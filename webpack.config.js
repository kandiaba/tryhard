module.exports = {
    entry: "./main.js",
    output: {
        filename: "bundle.js"
    },
    devServer: {
        inline: true,
        port: 3000,
        hot: true
    },
    watch: true,
    devtool: "eval-cheap-source-map"
}
