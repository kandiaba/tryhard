const hostels = {
    hotel1: {
        id: 1,
        name: 'hotel rose',
        roomNumbers: 10,
        pool: true,
        rooms:
            {
                room1: {
                    roomName: 'suite de luxe',
                    size: 2,
                    id: 1,
                },
                room2: {
                    roomName: 'suite nuptiale',
                    size: 2,
                    id: 2,
                },
                room3: {
                    roomName: 'suite familiale',
                    size: 4,
                    id: 3,
                },
                room4: {
                    roomName: 'suite budget',
                    size: 2,
                    id: 4,
                },
                room5: {
                    roomName: 'suite familiale',
                    size: 4,
                    id: 5,
                },
                room6: {
                    roomName: 'suite budget',
                    size: 3,
                    id: 6,
                },
                room7: {
                    roomName: 'suite de luxe',
                    size: 2,
                    id: 7,
                },
                room8: {
                    roomName: 'suite familiale',
                    size: 4,
                    id: 8,
                },
                room9: {
                    roomName: 'suite de luxe',
                    size: 3,
                    id: 9,
                },
                room10: {
                    roomName: 'suite présidentielle',
                    size: 5,
                    id: 10,
                },
            },

    },
    hotel2: {
        id: 2,
        name: 'hotel ocean',
        roomNumbers: 15,
        pool: false,
        rooms: {
            room1: {
                roomName: 'suite pacifique',
                size: 2,
                id: 1,
            },
            room2: {
                roomName: 'suite atlantique',
                size: 2,
                id: 2,
            },
            room3: {
                roomName: 'suite manche',
                size: 4,
                id: 3,
            },
            room4: {
                roomName: 'suite mer du nord',
                size: 2,
                id: 4,
            },
            room5: {
                roomName: 'suite pacifique',
                size: 4,
                id: 5,
            },
            room6: {
                roomName: 'suite mer du nord',
                size: 3,
                id: 6,
            },
            room7: {
                roomName: 'suite atlantique',
                size: 2,
                id: 7,
            },
            fkdjhkjsdh: {
                roomName: 'suite pacifique',
                size: 4,
                id: 8,
            },
            lfehfgkdjsdhfjd: {
                roomName: 'suite atlantique',
                size: 3,
                id: 9,
            },
            eofuhdkfdhksjfh: {
                roomName: 'suite atlantique',
                size: 5,
                id: 10,
            },
            flezhkfdjhkdgfskf: {
                roomName: 'suite pacifique',
                size: 2,
                id: 11,
            },
            zuhakahaljdlh: {
                roomName: 'suite mer du nord',
                size: 2,
                id: 12,
            },
            lfekjhljhdkjdfhfdkj: {
                roomName: 'suite manche',
                size: 4,
                id: 13,
            },
            zkoiuzaojaojaklj: {
                roomName: 'suite manche',
                size: 3,
                id: 14,
            },
            djkjfhkjfhfdkjhfdj: {
                roomName: 'suite mer du nord',
                size: 5,
                id: 15,
            },
        },
    },
    hotel3: {
        id: 3,
        name: 'hotel des Pins',
        roomNumbers: 7,
        pool: true,
        rooms: {
            ch1: {
                roomName: 'suite bordelaise',
                size: 2,
                id: 1,
            },
            ch2: {
                roomName: 'suite marseillaise',
                size: 2,
                id: 2,
            },
            ch23: {
                roomName: 'suite nicoise',
                size: 4,
                id: 3,
            },
            ch89: {
                roomName: 'suite canoise',
                size: 2,
                id: 4,
            },
            sshs: {
                roomName: 'suite hendaiar',
                size: 4,
                id: 5,
            },
            sdisduye: {
                roomName: 'suite canoise',
                size: 3,
                id: 6,
            },
            ksjqhfkhfskjdh: {
                roomName: 'suite nicoise',
                size: 2,
                id: 7,
            },
        },
    },
};

console.log(hostels);
// eslint-disable-next-line no-unused-vars
const keys = Object.keys(hostels);
console.log(keys);
const values = Object.values(hostels);
console.log(values);
// faire tous les exos de la page arrays en utilisant object.keys ou object.values

// eslint-disable-next-line no-unused-vars

// exercice 0 : mettre une majuscule à toutes les RoomName
console.log('______exo0_______');

// exercice 1 : trier les hotels par nombre
// de chambres (plus grand en 1er) et créer un tableau
// contenant seulement
// le nom des hotels dans leur ordre de tri
/*
console.log('______exo1_______');

// eslint-disable-next-line no-unused-vars
const hostelsArray = Object.values(hostels);
const sortdArray = hostelsArray.sort((a, b) =>b.roomNumbers -a.roomNumbers)
.map((hostel) =>hostel.name);
console.log(sortdArray);*/


// exercice 2 : faire un tableau avec toutes les chambres de tous les hotels,
// et ne garder que les chambres qui
// ont plus que 3 places ou exactement 3 places et
// les classer par ordre alphabétique selon le non de la chambre
console.log('______exo2_______');

// eslint-disable-next-line no-unused-vars
/* const hostelsdArrayRoomname = Object.values(hostels)
    .reduce((acc, room) => [...acc, ...Object.values(room.rooms)], []);
console.log(hostelsdArrayRoomname);
// eslint-disable-next-line no-unused-vars
const hostelsdArrayRoomnameFilter = hostelsdArrayRoomname.filter((room) => room.size>3);

console.log(hostelsdArrayRoomnameFilter);

const hostelsdArrayRoomnameSort = hostelsdArrayRoomnameFilter.sort((a, b) => a.roomName > b.roomName ? 1 : -1);
console.log(hostelsdArrayRoomnameSort);*/


// exercice 3 : faire un tableau avec toutes les chambres des hotels
// et ne garder que les chambres qui ont plus de 3 places et dont le
// nom de chambre a une taille supérieure à 15 charactères
console.log('______exo3_______');
/* const hosteldArray = Object.values(hostels);
console.log(hosteldArray);
const hostelsdArrayRoomLength = hosteldArray
    .reduce((acc, room) => [...acc, ...Object.values(room.rooms)], [])
    .filter((room) => room.size > 3)
    .filter((room) => room.roomName.length > 15);
console.log(hostelsdArrayRoomLength);*/


// exercice 4 : enlever de la liste des hotels toutes les chambres qui ont plus de 3 places et
// changer la valeur de roomNumbers pour qu'elle reflete
// le nouveau nombre de chambres
// eslint-disable-next-line max-len
console.log('______exo4_______');
/* const hostelsdArrayRoomFilterMap = Object.values(hostels)
    .reduce((acc, room) => [...acc, ...Object.values(room.rooms)], [])
    .filter((room) => room.size > 3)
    .map((room) => {
      room.roomNumbers = room.roomName.length;
      return room;
    });
console.log(hostelsdArrayRoomFilterMap);*/

// exercice 5  : extraire du tableau hostels l'hotel qui a le nom 'hotel ocean' en le supprimant du tableau,
// et le mettre dans une nouvelle variable
// puis effacer toutes ses chambres et mettre à jour sa valeur room number, puis pusher l'hotel modifié dans hostels
// puis faire un sort par nom d'hotel
// puis donner le nouvel index de l'hotel océan (faire 2 méthodes : avec indexOf et avec un foreach)
console.log('______exo5_______');

// const indexOfHostelOcean = Object.values(hostels).findIndex((hostel) => hostel.name === 'hotel ocean');
// console.log('index of hostel ocean', indexOfHostelOcean);
/* const [hostelOcean] = Object.values(hostels).splice(1, 1);
console.log(hostelOcean);
console.log(Object.values(hostels));
hostelOcean.rooms= [];
hostelOcean.roomNumbers = Object.values(hostelOcean.rooms).length;
console.log(hostelOcean);
Object.values(hostels).push(hostelOcean);
console.log( Object.values(hostels));
console.log(Object.values(hostels).indexOf(hostelOcean));*/

// exercice 6 : créer un objet dont les clés sont le nom des hotels et dont la valeur est un booléen qui indique
// si l'hotel a une chambre qui s'appelle 'suite marseillaise'
console.log('______exo6_______');
/* const hostelsdArray = Object.values(hostels);
console.log(hostelsdArray);
objHostel = {};

Object.values(hostels).forEach((hostel) =>{
  hostel.rooms = Object.values(hostel.rooms);
  objHostel[hostel.name] = hostel.rooms.some((room) => room.roomName === 'suite marseillaise');
  return hostel.name;
});
console.log(objHostel);*/
// exercice 7 : faire une fonction qui prend en paramètre un id d'hotel et qui retourne son nom
/* c onsole.log('______exo7_______');
function idHostel(ids) {
  const hostelName = Object.values(hostels)
      .find((hostel) =>hostel.id === ids);
  return hostelName ? hostelName.name : 'id not fund';
}
console.log(idHostel(0));
console.log(idHostel(3)); */
// exercice 8 : faire une fonction qui prend en paramètre un id d'hotel et un id de chambre et qui retourne son nom
/* console.log('______exo8_______');
function idHostelRoom(idHostel, idRoom) {
  const hostelName = Object.values(hostels).find((hostel) => hostel.id ===idHostel);
  if (!hostelName) {
    return 'pas dhotel';
  }
  const roomName = Object.values(hostelName.rooms).find((hostel) => hostel.id === idRoom);
  if (!roomName) {
    return 'pas de rooms';
  }
  return roomName.roomName;
}
console.log(idHostelRoom(1, 3));*/
// exercice 9: faire une fonction qui prend en paramètre la liste des hotels
// et qui vérifie que toutes les chambres des hotels ont bien
// une majuscule a leur nom et qui renvoie un boolean donnant le résultat.
// Puis faire en sorte que la liste des hotels valide bien cette fonction.
console.log('______exo9_______');
function capi(word) {
    if (typeof word !== 'string') {
        return '';
    }
    return word.charAt(0).toUpperCase() + word.slice(1);
}

function verifyHostelsRoom(listHostels) {
    return Object.values(listHostels).every((hostel) =>{
        return Object.values(hostel.rooms).every((room) => {
            return room.roomName === capi(room.roomName);
        });
    });
}
console.log(verifyHostelsRoom(hostels));

function capiHostelsRoom(listHostels) {
    return Object.values(listHostels).every((hostel) =>{
        Object.values(hostel.rooms).every((room) =>{
            room.roomName = capi(room.roomName);
        });
    });
}
console.log(capi('toto'));
console.log(capiHostelsRoom(hostels));
console.table(hostels);
// exercice 10 : faire une fonction qui prend en paramètre la liste des hotels
// et qui renvoie un tableau contenant les id des hotels
// qui ont au moins une chambre avec plus ou égale à 4 places
console.log('______exo10_______');
function listHostels(theHostel) {
    return Object.values(theHostel).filter(((hostel) =>
        Object.values(hostel.rooms)
        .some((room) =>room.size > 4)))
    .map((hostel) => hostel.id);
}
console.table(listHostels(hostels));


// exercice 11 : faire une fonction qui prend en paramètre la liste des hotels
// et qui renvoie toutes les chambres qui ont plus de 3 places, dans un hotel
// avec piscine et qui ajoute à chaque chambre une propriété "hotelName"
// qui contient le nom de l'hotel à laquelle elle appartient

// exercice 12 : faire une fonction qui prend en paramètre la liste des hotel,
// un id d'hotel et une taille et qui renvoie le nom et l'id de toutes
// les chambres de cet hotel qui font exactement la taille indiquée

// exercice 13 : faire une fonction qui prend en paramètre la liste des hotel et
// un id d'hotel et qui supprime cet hotel de la liste des hotels

// exercice 14 : faire une fonction qui prend en paramètre la liste des hotel et
// un id d'hotel et un id de chambre et qui supprime cet chambre de l'hotel concerné

// exercice 15 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel et un boolean et qui va changer la valeur de l'attribut "pool"
// dans l'hotel concerné avec le boolean donné

// exercice 16 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel et une nouvelle chambre (avec son nom et sa taille) qui
// ajoute la nouvelle chambre dans l'hote concerné et lui donne un id qui suit le
// dernier id de l'hotel

// exercice 17 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel, un id de chambre et un nouveau nom de chambre qui va changer le nom
// de la chambre indiquée par le nouveau nom

// faire tous les exos de la page arrays en utilisant object.keys ou object.values


// Faire les exercices suivants :
/*
const lessons = {
  lesson1: true,
  lesson11: false,
  lesson2: true,
  lesson15: false,
  lesson3: true,
  lesson4: true,
  lesson13: false,
  lesson6: false,
  lesson12: false,
  lesson7: false,
  lesson8: false,
  lesson9: false,
  lesson10: false,
  lesson5: false,
  lesson14: false,
};
// console.log(lessons);

// exercice 1 : changer cet objet pour que ses clés soient classées dans l'ordre alphabétiques
console.log(Object.keys(lessons));
const newKeys = Object.keys(lessons).sort((a, b) => a > b ? 1: -1);
console.log(newKeys);
const newLessons = {};
newKeys.forEach((key) => {
  newLessons[key] = lessons[key];
});

console.table(newLessons);

// exercice 2 : en gardant l'objet trié, faites une fonction qui
// donne le numéro de la première clé qui a la valeur false
// sous la forme {key: 4, value: false}
// eslint-disable-next-line no-unused-vars
function keyFalse() {
  const key = Object.values(newLessons).findIndex((value)=> value === false);
  return {key: key, value: false};
}
console.log(keyFalse());
*/


/// faire tous les exos suivants en utilisant Object.keys ou Object.values

const hostelValues = Object.values(hostels);
console.table(hostelValues);
const hostelsKeys = Object.keys(hostels);
console.table(hostelsKeys);


// exercice 1 : trier les hotels par nombre de chambres (plus grand en 1er) et créer un tableau contenant seulement

const hostelsTrie= Object.values(hostels).sort((h1, h2) => h1.roomNumbers - h2.roomNumbers)

// le nom des hotels dans leur ordre de tri

function compareFn() {

}

const hostelsNames= Object.values(hostels).sort( (h1, h2) => h1.roomNumbers - h2.roomNumbers).map(h =>h.name)
console.table(hostelsNames);


