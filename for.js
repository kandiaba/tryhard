// exercice 1 : créer une fonction qui prend en paramètre un nombre entier et qui
// affiche tous les entiers entre 0 et lui même
/*function nombreEntiers(nombre) {
  for (let i = 0; i <= nombre; i++) {
    console.log(i);
  }
}

console.log(nombreEntiers(4));

// exercice 2 : créer une fonction qui prend en paramètre 2 nombres entiers et qui
// affiche tous les entiers entre le premier et le second
function twoEvenNumbers(number1, number2) {
  for (let i = number1 + 1; i < number2; i++) {
    console.log(i);
  }
}
console.log(twoEvenNumbers(2, 4));
// exercice 3 : créer une fonction qui génére un tirage de loto avec
// 7 nombres compris entre 1 et 45
// Math.floor(n + Math.random() * (m - n)));
function loto() {
  const result = [];
  for (let i = 0; i < 7; i++) {
    result.push( 1 + Math.floor(Math.random()*45));
  }
  return result;
}

console.table(loto());
// exercice 4 : générer un tableau contenant des nombres pairs consécutifs,
// le premier nombre du tableau doit être 4,
// on doit arreter de remplir le tableau quand il y a 20 nombres pairs dans le tableau

function evenNumbers() {
  const result = [];
  for (let i = 4; ; i++) {
    if (i%2 === 0) {
      result.push(i);
    }

    if (result.length === 20) {
      return result;
    }
  }
}

console.table(evenNumbers());

*/

// exercice 5 : générer un array avec 100 objets avec la forme ci dessous, dont les données sont toutes aléatoires

// const person = {firstName: '', lastName: '', age: 0};
// console.log(person);

// bonus : calculer la moyenne des âges de personnes en utilisant un reduce
/*function randomObjects() {
  const result = [];
  for (let i = 0; i < 100; i++) {
    const person = {firstName: '', lastName: '', age: Math.floor(Math.random()*100)};
    person.firstName = (Math.random()*50).toFixed();
    person.lastName = (Math.random()*50).toFixed();
    result.push(person);
  }
  return result;
}

function ageMoyen(persons) {
  const totalAge = persons.reduce((acc, person) => acc + person.age, 0);
  return totalAge / persons.length;
}
const persons = randomObjects();
console.table(persons);
console.table(ageMoyen(persons));
*/
/*
const firstName = ['kandiaba','Nydhal', 'Farid','Faysal'];
const lastName = ['Traore',  'Diallo', 'Hormeli', 'Djego'];
function randomObjects() {
  const result = [];
  for (let i = 1; i <= 100; i++) {
    const person = {firstName: '', lastName: '', age: Math.floor(Math.random()*99)};
    const index = Math.floor(Math.random()*4);
    person.firstName = firstName[index];
    person.lastName = lastName[index];
    result.push(person);
  }
  return result;
}
console.table(randomObjects());
*/
/*
const persons = [
  {firstName:'kandiaba', lastName:'TRAORE'},
  {firstName:'Nydhal', lastName: 'Diallo'},
  {firstName:'Farid', lastName:'Faysal'},
  {firstName:'Jack', lastName:'Dupont'}];

function randomObjects() {
  const result = [];
  for (let i = 1; i <= 100; i++) {
    const person = {firstName: '', lastName: '', age: Math.floor(Math.random()*99)};
    const selectedPerson = persons[Math.floor(Math.random()*4)];
    person.firstName = selectedPerson.firstName;
    person.lastName = selectedPerson.lastName;
    result.push(person);
  }
  return result;
}
console.table(randomObjects());*/

// exercice 6 : Écrire un programme qui affiche les nombres de 1 à 199 avec
// un console log.
// Mais pour les multiples de 3, afficher “Fizz” au lieu du nombre et pour les multiples de 5 afficher “Buzz”.
// Pour les nombres multiples de 3 et 5, afficher “FizzBuzz”.
/*function f() {
  for (let i = 1; i <= 199; i++) {
    console.log(i);
    if (i % 3 === 0) {
      console.log('Fizz');
    }
    if (i % 5 === 0) {
      console.log('Buzz');
    }
    if (i % 3 === 0 && i % 5 === 0) {
      console.log('FizzBzz');
    }
  }
}

f();

function fizzBuzz(nombre) {
  if (nombre % 3 === 0) {
    return 'Fizz';
  }
  if (nombre % 5 === 0) {
    return 'Buzz';
  }
  if (nombre % 3 === 0 && nombre % 5 === 0) {
    return 'FizzBzz';
  }
}

console.log(fizzBuzz(3));*/
